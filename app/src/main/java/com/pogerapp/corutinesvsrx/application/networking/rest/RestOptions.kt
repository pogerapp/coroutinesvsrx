package com.pogerapp.corutinesvsrx.application.networking.rest

object RestOptions{
    const val HEADER_KEY_AUTH = "Authorization"
    const val HEADER_VALUE_BEARER = "Bearer"
    const val TIMEOUT = 15L
}