package com.pogerapp.corutinesvsrx.application.networking.repository

import com.pogerapp.corutinesvsrx.BuildConfig
import com.pogerapp.corutinesvsrx.application.StatusModel
import com.pogerapp.corutinesvsrx.application.networking.rest.RestClient
import com.pogerapp.corutinesvsrx.application.parse
import kotlinx.coroutines.Deferred
import retrofit2.Response

abstract class BaseRepository {

//    val restClient: RestClient by inje

    suspend inline fun <reified T> apiCall(noinline call: suspend () -> Deferred<Response<T>>): StatusModel<T> {
        val response = call.invoke().await()
        return when {
            response.isSuccessful -> StatusModel.success(response.body())
            BuildConfig.BUILD_TYPE == "mock" -> StatusModel.success(T::class.java.newInstance())
            else -> StatusModel.error(response.parse())
        }
    }
}