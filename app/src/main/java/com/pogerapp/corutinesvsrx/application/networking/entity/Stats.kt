package com.pogerapp.corutinesvsrx.application.networking.entity

import com.google.gson.annotations.SerializedName

data class Stats(
    @SerializedName("lossStreak")
    var lossStreak: Int? = 0,

    @SerializedName("winStreak")
    var winStreak: Int? = 0,

    @SerializedName("lifetimeGold")
    var lifetimeGold: Float? = 0f
)