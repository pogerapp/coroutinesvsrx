package com.pogerapp.corutinesvsrx.application.di

import com.pogerapp.corutinesvsrx.application.networking.repository.player.PlayerRepository
import com.pogerapp.corutinesvsrx.application.networking.repository.player.playerRepository
import org.koin.dsl.module.module

val repositoryModule = module {
    single { PlayerRepository(get()) }
}