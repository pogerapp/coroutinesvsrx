package com.pogerapp.corutinesvsrx.application

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.res.Resources
import android.util.TypedValue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Response
import java.lang.Exception

val Int.dp: Int
        get() = (Resources.getSystem().displayMetrics.density).toInt() * this

fun <T> runCorutine(liveData: MutableLiveData<StatusModel<T>>, block: suspend () -> T?) {
    liveData.value = StatusModel.loading()

    GlobalScope.launch(Dispatchers.IO) {
        try {
            liveData.postValue(StatusModel.success(block()))
        } catch (ex: Exception) {
            liveData.postValue(StatusModel.error(ex))
        }
    }
}

fun <T> Response<T>.parse() = Exception("Handled stub")
