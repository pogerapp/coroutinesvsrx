package com.pogerapp.corutinesvsrx.application.utils

import android.content.Context
import java.io.IOException
import java.io.InputStream
import android.graphics.*




object BitmapUtils {

    fun getImage(context: Context): Bitmap {
        val assetManager = context.assets
        var istr: InputStream? = null
        try {
            istr = assetManager.open("TODO.jpg")
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return BitmapFactory.decodeStream(istr)
    }

    fun processBitmap(bitmap: Bitmap, width: Int, height: Int, corners: Int): Bitmap? {
        val output = Bitmap.createBitmap(
            width,
            height, Bitmap.Config.ARGB_8888
        )

        val input = Bitmap.createScaledBitmap(bitmap, width, height, false)

        val canvas = Canvas(output)

        val color = -0xbdbdbe
        val paint = Paint()
        val rect = Rect(0, 0, input.width, input.height)
        val rectF = RectF(rect)

        paint.isAntiAlias = true
        canvas.drawARGB(0, 0, 0, 0)
        paint.color = color
        canvas.drawRoundRect(rectF, corners.toFloat(), corners.toFloat(), paint)

        paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
        canvas.drawBitmap(input, rect, rect, paint)

        return output
    }

    suspend fun getImageSus(context: Context) = getImage(context)
    suspend fun processBitmapSus(bitmap: Bitmap, width: Int, height: Int, corners: Int)
            = processBitmap(bitmap, width, height, corners)

}