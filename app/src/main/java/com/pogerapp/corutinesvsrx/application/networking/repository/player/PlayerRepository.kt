package com.pogerapp.corutinesvsrx.application.networking.repository.player

import com.pogerapp.corutinesvsrx.application.StatusModel
import com.pogerapp.corutinesvsrx.application.networking.entity.Player
import com.pogerapp.corutinesvsrx.application.networking.repository.BaseRepository
import com.pogerapp.corutinesvsrx.application.networking.rest.RestClient

fun playerRepository(restClient: RestClient) = PlayerRepository(restClient)


class PlayerRepository(private var restClient: RestClient): BaseRepository() {
    suspend fun getPlayerByName(playerName: String): StatusModel<Player> {
        return apiCall { restClient.getPlayer(arrayListOf(playerName)) }
    }
}