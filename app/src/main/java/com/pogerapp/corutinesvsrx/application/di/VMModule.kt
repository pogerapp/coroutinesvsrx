package com.pogerapp.corutinesvsrx.application.di

import com.pogerapp.corutinesvsrx.ui.MainViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.applicationContext
import org.koin.dsl.module.module

val vmModule = module {
    viewModel { MainViewModel(androidApplication(), get()) }
}