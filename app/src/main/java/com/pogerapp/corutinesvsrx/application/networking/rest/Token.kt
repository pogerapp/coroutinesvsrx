package com.pogerapp.corutinesvsrx.application.networking.rest

import android.content.Context

private const val TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9." +
        "eyJqdGkiOiIyZmEzYzM4MC02MGYzLTAxMzYtMzRhMS0wYTU4NjQ2MGI5NDMi" +
        "LCJpc3MiOiJnYW1lbG9ja2VyIiwiaWF0IjoxNTMwNjI0NjMzLCJwdWIiOiJz" +
        "ZW1jIiwidGl0bGUiOiJ2YWluZ2xvcnkiLCJhcHAiOiJmOThmYWM3MC0wYzc2LTAx" +
        "MzUtODgyMy0wMjQyYWMxMTAwMDkiLCJzY29wZSI6ImNvbW11bml0eSIsImxpbWl0Ijo4MDB9." +
        "2oHiBSX5WwamOCm0WgZOg-E23sQpApFzvZrD0vAYV90"
class Token(var context: Context) {
    private val key = "TOKEN"
    private val rtKey = "TOKEN"
    private val prefsManager = context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)!!

    var token: String?
        set(value) = prefsManager.edit().putString(key, value).apply()
        get() = prefsManager.getString(key, TOKEN)

    var refreshToken: String?
        set(value) = prefsManager.edit().putString(rtKey, value).apply()
        get() = prefsManager.getString(rtKey, null)
}