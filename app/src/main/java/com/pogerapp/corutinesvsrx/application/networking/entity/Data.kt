package com.pogerapp.corutinesvsrx.application.networking.entity

import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("type")
    var type: DataType? = null,

    @SerializedName("id")
    var id: String? = null,

    @SerializedName("attributes")
    var attributes: Attributes? = null
) {
    enum class DataType(type: String) {
        PLAYER("player")
    }
}