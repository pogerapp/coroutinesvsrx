package com.pogerapp.corutinesvsrx.application

class StatusModel<T>(var status: Status, var data: T? = null, var error:Throwable? = null) {


    enum class Status{
        SUCCESS,
        ERROR,
        LOADING
    }

    companion object {
        fun <T> error(throwable: Throwable) = StatusModel<T>(Status.ERROR, error = throwable)

        fun <T> loading() = StatusModel<T>(Status.LOADING)

        fun <T> success(data: T?) = StatusModel<T>(Status.SUCCESS, data)
    }
}