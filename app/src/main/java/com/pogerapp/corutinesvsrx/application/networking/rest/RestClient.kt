package com.pogerapp.corutinesvsrx.application.networking.rest

import com.pogerapp.corutinesvsrx.application.networking.entity.Player
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface RestClient {
    @GET("eu/players")
    fun getPlayer(@Query("filter") playerName: ArrayList<String>): Deferred<Response<Player>>
}