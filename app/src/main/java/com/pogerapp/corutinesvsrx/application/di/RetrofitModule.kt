package com.pogerapp.corutinesvsrx.application.di

import com.google.gson.GsonBuilder
import com.pogerapp.corutinesvsrx.BuildConfig
import com.pogerapp.corutinesvsrx.application.networking.repository.player.playerRepository
import com.pogerapp.corutinesvsrx.application.networking.rest.RestClient
import com.pogerapp.corutinesvsrx.application.networking.rest.RestOptions
import com.pogerapp.corutinesvsrx.application.networking.rest.Token
import com.pogerapp.corutinesvsrx.ui.MainViewModel
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


val retrofitModule = module {
    single { Token(androidContext()) }
    single { getAuthInterceptor(get()) }
    single { getClient(get()) }
    single { getRetrofit(get()) }
    single { getApiClient(get()) }
}

private fun getApiClient(retrofit: Retrofit) = retrofit.create(RestClient::class.java)

private fun getRetrofit(client: OkHttpClient) = Retrofit.Builder()
    .baseUrl(BuildConfig.BASE_URL)
    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
    .addConverterFactory(GsonConverterFactory.create(getGson()))
    .client(client)
    .build()

private fun getClient(authInterceptor: Interceptor) = OkHttpClient.Builder()
    .addInterceptor(authInterceptor)
    .retryOnConnectionFailure(true)
    .connectTimeout(RestOptions.TIMEOUT, TimeUnit.SECONDS)
    .readTimeout(RestOptions.TIMEOUT, TimeUnit.SECONDS)
    .writeTimeout(RestOptions.TIMEOUT, TimeUnit.SECONDS)
    .build()

fun getAuthInterceptor(token: Token): Interceptor {
    return Interceptor { chain ->
        if (token.token != null) {
            val originalRequest = chain.request()
            val builder = originalRequest.newBuilder()

            if (originalRequest.header(RestOptions.HEADER_KEY_AUTH) == null) {
                builder.header(RestOptions.HEADER_KEY_AUTH, RestOptions.HEADER_VALUE_BEARER + token.token)
            }
            chain.proceed(builder.build())
        } else {
            val originalRequest = chain.request()
            chain.proceed(originalRequest)
        }
    }
}

private fun getGson() = GsonBuilder().create()
