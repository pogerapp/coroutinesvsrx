package com.pogerapp.corutinesvsrx.application.networking.entity

import com.google.gson.annotations.SerializedName

data class Attributes(
    @SerializedName("name")
    var name: String? = null,

    @SerializedName("patchVersion")
    var pathchVersion: String? = null,

    @SerializedName("shardId")
    var shardId: String? = null,

    @SerializedName("titleID")
    var titleID: String? = null,

    @SerializedName("stats")
    var stats: Stats? = null)