package com.pogerapp.corutinesvsrx.application

import android.app.Application
import com.pogerapp.corutinesvsrx.application.di.repositoryModule
import com.pogerapp.corutinesvsrx.application.di.retrofitModule
import com.pogerapp.corutinesvsrx.application.di.vmModule
import org.koin.android.ext.android.startKoin

class App: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(retrofitModule, vmModule, repositoryModule))
    }
}