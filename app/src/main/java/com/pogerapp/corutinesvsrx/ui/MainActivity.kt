package com.pogerapp.corutinesvsrx.ui

import android.arch.lifecycle.Observer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.pogerapp.corutinesvsrx.R
import com.pogerapp.corutinesvsrx.application.dp
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.android.architecture.ext.getViewModel
import org.koin.android.architecture.ext.viewModel
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

    var mainViewModel : MainViewModel? = null
    var disposable: Disposable? = null
    var timeout = 15

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViewModel()
        initClicks()
    }

    private fun initViewModel() {

        mainViewModel = getViewModel<MainViewModel>()

        mainViewModel?.bitmapCorLiveData?.observe(this, Observer {
            corImage.setImageBitmap(it?.data)
        })
        mainViewModel?.bitmapRxLiveData?.observe(this, Observer {
            rxImage.setImageBitmap(it?.data)
        })
    }

    private fun initClicks() {
        btn.setOnClickListener {
            mainViewModel?.makeImage(rxImage.width, corImage.height, 22.dp)
            mainViewModel?.getPlayer("poger")
            startTimers()
        }
    }

    private fun startTimers(){
        startTimerRx()
        startTimerCor()
    }

    private fun startTimerRx() {
        var temp = timeout
        disposable = Observable.interval(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if(temp > 0) {
                    temp--
                    Log.wtf("RX", temp.toString())
                }
                else {
                    disposable?.dispose()
                }
            }, {})
    }

    private fun startTimerCor() {
        var temp = timeout.toLong()
        GlobalScope.launch(Dispatchers.IO) {
            while (temp > 0) {
                Thread.sleep(1000)
                withContext(Dispatchers.Main) {
                    temp--
                    Log.wtf("COR", temp.toString())
                }
            }
        }
    }
}
