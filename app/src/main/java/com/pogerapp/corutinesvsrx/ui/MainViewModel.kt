package com.pogerapp.corutinesvsrx.ui

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.graphics.Bitmap
import com.pogerapp.corutinesvsrx.application.StatusModel
import com.pogerapp.corutinesvsrx.application.networking.entity.Player
import com.pogerapp.corutinesvsrx.application.networking.repository.player.PlayerRepository
import com.pogerapp.corutinesvsrx.application.networking.rest.RestClient
import com.pogerapp.corutinesvsrx.application.runCorutine
import com.pogerapp.corutinesvsrx.application.utils.BitmapUtils
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import java.lang.Exception

class MainViewModel(application: Application, val playerRepository: PlayerRepository) : AndroidViewModel(application) {

    var disposable = CompositeDisposable()

    val bitmapRxLiveData = MutableLiveData<StatusModel<Bitmap>>()
    val bitmapCorLiveData = MutableLiveData<StatusModel<Bitmap>>()
    val playerLiveData = MutableLiveData<StatusModel<Player>>()

    fun makeImage(width: Int, height: Int, corners: Int) {
        makeImageRx(width, height, corners)
        makeImageCor(width, height, corners)
    }

    fun getPlayer(name:String){
        runCorutine(playerLiveData){
            playerRepository.getPlayerByName(name).data
        }
    }

    private fun makeImageRx(width: Int, height: Int, corners: Int) {
        disposable.add(Single.create<Bitmap?> {
            try {
                val temp = BitmapUtils.processBitmap(BitmapUtils.getImage(getApplication()), width, height, corners)
                if (temp == null) it.onError(Exception("Empty image"))
                else it.onSuccess(temp)
            } catch (ex: Exception) {
                it.onError(ex)
            }
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { bitmapRxLiveData.value = StatusModel.loading() }
            .subscribe({
                bitmapRxLiveData.value = StatusModel.success(it)
            }, {
                bitmapRxLiveData.value = StatusModel.error(it)
            }))
    }

    private fun makeImageCor(width: Int, height: Int, corners: Int) {
        runCorutine(bitmapCorLiveData) {
            BitmapUtils.processBitmap(BitmapUtils.getImage(getApplication()), width, height, corners)
        }
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }
}